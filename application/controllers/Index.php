<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database('default');	
		$this->load->helper('url');	
		$this->load->library('session');
		//Load another database
		$DB2 = $this->load->database('another_db', TRUE);
		//echo '<h3>Testing hooks in constructor</h3><br>';
		
	}
	function Index()
	{
		//echo 'Test Admin Index';exit();
		$this->load->view('Template/header');		
		$this->load->view('Front/index');
		$this->load->view('Template/footer');
	}
	function About()
	{
		//echo 'Test Admin Index';exit();
		$this->load->view('Template/header');		
		$this->load->view('Front/about');
		$this->load->view('Template/footer');
	}
	function Products()
	{
		//echo 'Test Admin Index';exit();
		$this->load->view('Template/header');		
		$this->load->view('Front/products');
		$this->load->view('Template/footer');
	}
	function Contact()
	{
		//echo 'Test Admin Index';exit();
		$this->load->view('Template/header');		
		$this->load->view('Front/contact');
		$this->load->view('Template/footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */