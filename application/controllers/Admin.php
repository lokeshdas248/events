<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->database('default');	
		$this->load->helper('url');	
		$this->load->library('session');
        $this->load->model('admin/Users');
        $this->load->helper("URL", "DATE", "URI", "FORM");
        $this->load->library('pagination');

    }
    function Index_old()
	{
		//echo 'Test Admin Index';exit();
		$this->load->view('Template/header');		
		$this->load->view('Admin/login');
		$this->load->view('Template/footer');
	}
	function Dashboard()
	{	
		//echo $this->session->userdata('username');die();
		$data['sess_username'] = $this->session->userdata('username');
		//echo $_SESSION['username'];exit;
		$this->load->view('Template/header', $data);
		$this->load->view('Admin/dashboard', $data);
		$this->load->view('Template/footer', $data);
	}
	function Events()
	{
		$data['sess_username'] = $this->session->userdata('username');
		$data['res_prod'] = $this->Users->getEventData();
		$this->load->view('Template/header', $data);
		$this->load->view('Admin/events', $data);
		$this->load->view('Template/footer', $data);
	}
	public function Add_Event()
	{
		$data['sess_username'] = $this->session->userdata('username');
		$event_id=$this->input->post('hdn_event_id');
		if(count($_POST) > 0){
			if($event_id != ''){
				$status = $this->Users->update_event();
				if($status == 'Duplicate'){
					$this->session->set_flashdata('add_emp', 'Duplicate Event Code! Record can\'t be
					inserted.');		
					redirect(base_url() . 'admin/edit_event/'.$event_id);
					//$this->session->keep_flashdata('duplicate_emp');

				}
				else
				{				
					$this->session->set_flashdata('add_emp', 'Event has been updated successfully.');		
					redirect(base_url() . 'admin/events');
					//$this->session->keep_flashdata('add_emp');
				}
			}
			else
			{
				$status = $this->Users->insert_event();	
				//echo $this->db->last_query();exit();

				if($status == 'Duplicate'){
					$this->session->set_flashdata('add_emp', 'Duplicate Event Code! Record can\'t be
					inserted.');		
					redirect(base_url() . 'admin/add_event');
					//$this->session->keep_flashdata('duplicate_emp');

				}
				else
				{				
					$this->session->set_flashdata('add_emp', 'Event has been added successfully.');		
					redirect(base_url() . 'admin/events');
					//$this->session->keep_flashdata('add_emp');
				}
			}
		}
		$data['action'] = 'add';		
		$this->load->view('Template/header', $data);
		$this->load->view('Admin/add_event', $data);
		$this->load->view('Template/footer', $data);
	}
	public function edit_event($event_id){
		//echo $param1;exit;
		//Array ( [event_id] => 1 [event_name] => Test Event [event_code] => EVT001 [country] => India [city] => Delhi [address] => Golden Tree Park, Sector-5 Dwarka, West Delhi [phone] => 1111111111 [date_time] => 2021-12-30 08:54:11 [event_details] => t [is_active] => 1 )
		$data['res_event'] = $this->Users->Edit_event($event_id);
		$data['action'] = 'edit';
		$this->load->view('Template/header', $data);
		$this->load->view('Admin/add_event', $data);
		$this->load->view('Template/footer', $data);
	}
	public function delete_event($event_id){
		//echo $event_id;exit;
		$this->Users->Delete_event($event_id);
		$this->session->set_flashdata('del_event', 'Event has been deleted successfully.');		
		redirect(base_url() . 'admin/events');
	}
    public function login() {
    	$user = $this->input->post('username');
    	$password = $this->input->post('password');

        if($user != '' && $password !='') { 
		    //print_r($_POST);exit;
			$result = $this->Users->loginvalidate();
	        $data['users']= $result;
			//echo $result[0]->pk_user_id; exit();          
	        if($result!== '') {
	        	redirect( base_url().'admin/events');
	        }  
	        else  
	        {		
				$this->session->set_flashdata('user_error_msg',"Invalid Login or Password.!");	
	            redirect( base_url().'index.php/login/index');
	            
			}
			$this->load->view('admin/login_view');
		}
    }
    function loginform()
    { 
		if(isset($_POST['login']) && $_POST['login']!='')
		{ 
	  		///echo"first";
	 		/// exit;
          	$data['ResAre']=$this->user->loginvalidate();
          	if($data['ResAre'])
          	{
              	redirect( base_url().'index.php/admin/manage');
             
          	} else
          	{
				//echo"second";
			 	//exit;
             	redirect( base_url().'index.php/login/index');
            
         	}
        }
		 $this->load->view('admin/login_view');
    }
}