<div class="text-success" style="text-align:center">
	<?php echo $this->session->flashdata('add_event');?>
	<?php echo $this->session->flashdata('del_event');?>
</div>

<div class="bs-example widget-shadow" data-example-id="contextual-table">
	<h4>Manage Events</h4>
	<a href="<?php echo base_url().'admin/Add_Event'?>" id='add-event' class='add-customer-pop btn btn-raised btn-warning pull-right'>Add New</a>
	<table class="table" id="example1">						
		<thead>
			<tr>
			<th>CODE</th>
			<th>EVENT</th>						
			<th>DATE TIME</th>
			<th>CITY</th>
			<th>COUNTRY</th>
			<th>Address</th>							
			<th>Action</th>
			</tr>
		</thead>
		<tbody>								   
<?php
$i=0;
foreach($res_prod as $row)
{
	$i++;   
?>
			<tr class="active">
				<td><?php echo $row['event_code']?></td>
				<td><?php echo $row['event_name'];?></td>
				<td><?php echo $row['date_time']?></td>								
				<td><?php echo $row['city']?></td>
				<td><?php echo $row['country']?></td>
				<td><?php echo $row['address']?></td>			
				<td><a href="<?php echo base_url().'admin/edit_event/'.$row['event_id']?>">Edit</a> 
					<a class="text-danger" 
					href="<?php echo base_url().'admin/delete_event/'.$row['event_id']?>"
						data-toggle="modal" data-target="<?php echo '#deleteevent_'.$row['event_id'];?>" id="deleteevent"  onclick="return confirm('Are you sure to delete this event?')">Del</a>
					
				</td>
			</tr>
<?php 
}
?>	
		</tbody>
	</table>
			