<section id="main">
	<div class='inner' style="clear:both;min-height:400px;">
		<div class='col-md-12' style="padding:20px;"> 
			<div class='col-md-4'> </div>     
			<div class='col-md-4' style="border: 1px solid #e0dfdf;">
				<h2><center>Admin Login<br></center></h2>
				<p class='text-center'></p>
				<div class="inputs">
					<form method="post"  name="login" action="<?php echo base_url();?>admin/login" id="login_form">
					<div style="display:block">
						<div class="form-group label-floating">
							<label for="f2">Username</label>
							<input name='username' type="text" class="form-control" id="username" required >
							<span class="help-block"></span>
						</div>
						<div class="form-group label-floating">
							<label for="f1">Password</label>
							<input type="password" name='password' class="form-control" id="password" required>
							<span class="help-block"></span>
						</div>
						<br>
						<button type='submit' class='btn btn-raised btn-block btn-success btn-bm' name="submit" id='button1'><span class='glyphicon glyphicon-lock'></span> LOGIN</button><br>
					</div>
					<?php if($this->session->flashdata("login_fail")){?>
					<div class="alert alert-dismissible alert-danger js-valid"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Invalid login.
					</div>	
					<?php } ?>
					</form>
				</div>
			</div>
			<div class='col-md-4'> </div>
		</div>
	</div>
</section>