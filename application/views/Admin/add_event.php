<h4><?php echo ($action == 'edit')?'Update Event':'Add Event'?></h4>
<h5 class="text-danger"><?php echo $this->session->flashdata('add_emp');?></h5>
<form name="frm_add_prod" method="post" action="<?php echo base_url()?>admin/add_event">
	<input type="hidden" name="hdn_event_id" value="<?php echo (isset($res_event['event_id']))?$res_event['event_id']:''?>">
<div class="col-md-12">	
	<div class="row">
		<div class="col-md-6">			
			<div class="group">	
				<label class="hrmsLabel required">Event Name</label>										
				<input class="inputMaterial" type="text" id="event_name" name="event_name" required="required" value="<?php echo (isset($res_event['event_name']))?$res_event['event_name']:''?>">					
			</div>			
		</div>
		<div class="col-md-6">			
			<div class="group">
				<label class="hrmsLabel required">Date Time</label>
				<input class="inputMaterial" type="datetime-local" id="date" name="date_time" value="<?php echo (isset($res_event['date_time']))?$res_event['date_time']:''?>">				
			</div>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">			
			<div class="group">	
				<label class="hrmsLabel required">Event Code</label>										
				<input class="inputMaterial" type="text" id="event_code" name="event_code" required="required" value="<?php echo (isset($res_event['event_code']))?$res_event['event_code']:''?>">					
			</div>			
		</div>
		<div class="col-md-6">			
			<div class="group">
				<label class="hrmsLabel required">Address</label>
				<input class="inputMaterial" type="text" id="address" name="address" required="required" value="<?php echo (isset($res_event['address']))?$res_event['address']:''?>">				
			</div>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">			
			<div class="group">	
				<label class="hrmsLabel required">City</label>										
				<input class="inputMaterial" type="text" id="city" name="city" value="<?php echo (isset($res_event['city']))?$res_event['city']:''?>">					
			</div>			
		</div>
		<div class="col-md-6">			
			<div class="group">
				<label class="hrmsLabel required">Country</label>
				<select class="inputMaterial" name="country" id="country">
					<option value="India" <?php echo (isset($res_event['country']) && $res_event['country']=='India' )?'selected':''?>>India</option>
					<option value="USA"  <?php echo (isset($res_event['country']) && $res_event['country']=='USA' )?'selected':''?>>USA</option>
					<option value="Canada" <?php echo (isset($res_event['country']) && $res_event['country']=='Canada' )?'selected':''?>>Canada</option>
					<option value="France" <?php echo (isset($res_event['country']) && $res_event['country']=='France' )?'selected':''?>>France</option>
				</select>				
			</div>			
		</div>
	</div>
	<div class="row">		
		<div class="col-md-6">			
			<div class="group">	
				<label class="hrmsLabel required">Detail</label>										
				<textarea class="inputMaterial" rows="5" type="text" id="event_details" name="event_details"><?php echo (isset($res_event['event_details']))?$res_event['event_details']:''?></textarea>					
			</div>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-6" style="padding: 10px;">			
			<button type="submit" class="btn md-btn btn-primary btn-sm"><?php echo ($action == 'edit')?'Update Event':'Add Event'?></button>
			<a href="<?php echo base_url().'admin/Events'?>" class="btn btn-warning btn-md" style="padding:10px">Cancel</a>

		</div>
	</div>						
</div>	
</form>