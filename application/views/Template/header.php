<!DOCTYPE HTML>
<!--
	Introspect by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>MVC Application in CI</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css" />
		 
	    
	    <!--Bootstrap-->
	    <link rel="stylesheet" href="<?php echo base_url()?>bootstrap.min.css" type="text/css" />
	    <script type="text/javascript" src="<?php echo base_url()?>bootstrap.min.js"></script>
	    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/styles.css" type="text/css" />
	    <script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	    <script>
	    	$(document).ready(function(){
	    		//Modal
				//-----------------------------------------------
				if($(".modal").length>0) {
					$(".modal").each(function() {
						$(".modal").prependTo( "body" );
					});
				}	
	    	})
	    	
	    </script>
	    	
	</head>
	<body>
		<!-- Header -->
		<header id="header">
			<div class="inner">
				<a href="index.html" class="logo">Logo here</a>
				<nav id="nav">
					<a href="<?php echo base_url()?>">Home</a>
					
				</nav>
			</div>
		</header>
		<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

		<!-- Banner -->
		<!--<section id="banner">
			<div class="inner">
				<h1>Introspect: <span>A free + fully responsive<br />
				site template by TEMPLATED</span></h1>
				<ul class="actions">
					<li><a href="#" class="button alt">Get Started</a></li>
				</ul>
			</div>
		</section>-->
		<?php 
			if(isset($sess_username) && $sess_username != ''){
		?>
			<section id="main">
				<div class='inner inner_content'>
					<div class='col-md-12'> 
						<div class='col-md-2 left_menu'>
							<ul>
								<li>Products</li>
								<li>User</li>
								<li>Pages</li>
							</ul>
						 </div>     
						<div class='col-md-10'>
		<?php
			}
			else
			{
		?>
			<section id="main">
				<div class='inner inner_content'>
					<div class='col-md-12'> 						     
						
		<?php
			}
		?>

		

