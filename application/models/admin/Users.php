<?php
class Users extends CI_Model
{
    function loginvalidate()
    {
       
        $username=$this->input->post('username', TRUE);
        $pwd=$this->input->post('password', TRUE);     
        //$query = $this->db->query("select * from users where username= '".$username. "' and binary password= '". $pwd . "'");
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('username ', trim($username));
        $this->db->where('password', trim($pwd));
               
    	  $result='';        
        $query = $this->db->get();        
        //echo $this->db->last_query(); die;
        //echo $query->num_rows(); die;

        if($query->num_rows==1)
        {
            $result=$query->result();    		    
            $se_data=array('pk_user_id'=>$result[0]->pk_user_id,
                          'username'=>$result[0]->email);  
            $this->session->set_userdata($se_data);
            return  $result;
        }
        else
        { return $result; }
    }

    function insert_event(){
        //print_r($_POST);exit();        
        ///to insert data in add event        
        $event_name=$this->input->post('event_name');
        $date_time=$this->input->post('date_time');
        $event_code=$this->input->post('event_code');     
        $city=$this->input->post('city');        
        $country=$this->input->post('country');
        $address=$this->input->post('address');
        $event_details=$this->input->post('event_details');
        //$phone=$this->input->post('phone');        
        //print_r($_POST);exit();
        //INSERT INTO `events` (`event_id`, `event_name`, `event_code`, `country`, `city`, `address`, `phone`, `date_time`, `event_details`, `is_active`) VALUES (NULL, 'Test Event', 'EVT001', 'India', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '1111111111', '2021-12-23 06:54:11.000000', 'test', '1');

        ////Array ( [event_name] => test [date_time] => 2022-01-06T14:40 [event_code] => EVE012 [address] => test address [city] => Odisha [country] => India [phone] => 1111111111 [event_details] => test )
        
       $this->db->select('*');
       $this->db->from('events');
       $this->db->where('event_code',$event_code);
       $query=$this->db->get();            
       if ($query->num_rows() > 0)
       {
            return 'Duplicate';
       } 
       else
       {
            $data=array(
                'event_name'=>$event_name,
                'event_code'=>$event_code,
                'city'=>$city,
                'address'=>$address,                
                'date_time'=>$date_time,                
                'event_details'=>$event_details,                                
            );
            $this->db->insert('events',$data);
            //echo $this->db->last_query(); exit();
            return 'Success';
       }
        
    }
    function update_event()
    {
        $event_id=$this->input->post('hdn_event_id');
        $event_name=$this->input->post('event_name');
        $date_time=$this->input->post('date_time');
        $event_code=$this->input->post('event_code');     
        $city=$this->input->post('city');        
        $country=$this->input->post('country');
        $address=$this->input->post('address');
        $event_details=$this->input->post('event_details');
        //$phone=$this->input->post('phone');        
        //print_r($_POST);exit();
        //INSERT INTO `events` (`event_id`, `event_name`, `event_code`, `country`, `city`, `address`, `phone`, `date_time`, `event_details`, `is_active`) VALUES (NULL, 'Test Event', 'EVT001', 'India', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '1111111111', '2021-12-23 06:54:11.000000', 'test', '1');

        ////Array ( [event_name] => test [date_time] => 2022-01-06T14:40 [event_code] => EVE012 [address] => test address [city] => Odisha [country] => India [phone] => 1111111111 [event_details] => test )
        
       $this->db->select('*');
       $this->db->from('events');
       $this->db->where('event_code',$event_code);
       $this->db->where('event_id !=',$event_id);
       $this->db->where('is_active',1);
       $query=$this->db->get();            
       if ($query->num_rows() > 0)
       {
            return 'Duplicate';
       } 
       else
       {
            $data=array(
                'event_name'=>$event_name,
                'event_code'=>$event_code,
                'city'=>$city,
                'address'=>$address,
                'event_details'=>$event_details,                                
            );
            $this->db->where('event_id',$event_id);
            $this->db->update('events',$data);
            
            //echo $this->db->last_query(); exit();
            return 'Success';
       }

    }
     function getEventData(){
        $this->db->select('*');
        $this->db->from('events');
        $this->db->where('is_active',1);
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    function Delete_event($event_id){
        $data = array('is_active'=>0);
        $this->db->where('event_id', $event_id);
        $this->db->update('events', $data);        
    }
    function Edit_event($event_id){
        $this->db->select('*');
        $this->db->from('events');
        $this->db->where('event_id',$event_id);
        $query = $this->db->get();
        return $result = $query->row_array();
    }
}
?>
