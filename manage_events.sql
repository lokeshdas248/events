-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2021 at 12:10 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvctest`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `dept` varchar(22) NOT NULL,
  `state` varchar(22) NOT NULL,
  `city` varchar(22) NOT NULL,
  `designation` varchar(22) NOT NULL,
  `email` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_code` varchar(50) NOT NULL,
  `country` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `date_time` datetime NOT NULL,
  `event_details` text NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `date_created` date NOT NULL DEFAULT current_timestamp(),
  `date_updated` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `event_name`, `event_code`, `country`, `city`, `address`, `phone`, `date_time`, `event_details`, `is_active`, `date_created`, `date_updated`) VALUES
(1, 'Test Event Update1', 'EVT003', 'India', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '1111111111', '2021-12-28 20:17:00', 'test update 1', 0, '2021-12-23', '2021-12-23'),
(2, 'Test Event Update2', 'EVT005', 'India', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '1111111111', '2021-12-30 20:19:00', 'test update 1', 1, '2021-12-23', '2021-12-23'),
(3, 'Test Event Update', 'EVT005', 'India', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '1111111111', '2021-12-30 20:19:00', 'test update 1', 0, '2021-12-23', '2021-12-23'),
(4, 'Test Event Update', 'EVT005', 'India', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '1111111111', '2021-12-30 20:19:00', 'test update 1', 0, '2021-12-23', '2021-12-23'),
(5, 'Test Event Update', 'EVT005', '', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '', '2021-12-30 20:19:00', 'test update 1', 0, '2021-12-23', '2021-12-23'),
(6, 'Test Event Update3', 'EVT004', '', 'Delhi', 'Golden Tree Park, Sector-5 Dwarka, West Delhi', '', '2021-12-28 18:19:00', 'test update 1', 1, '2021-12-23', '2021-12-23'),
(7, 'Test Event Update56', 'EVT008', '', 'bbsr', 'test address', '', '2022-01-05 20:57:00', 'testttttttttttttttttttt', 1, '2021-12-23', '2021-12-23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `pk_user_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(22) NOT NULL,
  `full_name` varchar(44) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`pk_user_id`, `username`, `password`, `full_name`, `user_email`, `is_active`) VALUES
(1, 'admin', 'password', 'Lokesh Das', 'Lokeshdas248@gmail.com', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`pk_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `pk_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
